package com.atlassian.stash.plugin.editor.io;

import com.atlassian.stash.scm.CommandOutputHandler;
import com.atlassian.utils.process.StringOutputHandler;

public class StringCommandOutputHandler extends StringOutputHandler implements CommandOutputHandler<String> {
}
